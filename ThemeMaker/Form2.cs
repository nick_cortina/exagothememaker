﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;
namespace ThemeMaker
{
    public partial class Form2 : Form
    {
        public string installPath;
        public string theme;
        public Form2()
        {
            InitializeComponent();
        }
        public Form2(string exagoPath, string themeName)
        {
            InitializeComponent();
            //have the user select the config they want to apply the theme to
            label1.Text = String.Format("Select a config file to apply {0} \r\n Default: WebReports.xml", themeName);
            installPath = exagoPath + @"\Config\";
            theme = themeName;
            string[] files;
            //find all XML files in the config directory to display to the user
            if(Directory.Exists(installPath))
            {
                files = Directory.GetFiles(installPath, @"*.xml", SearchOption.TopDirectoryOnly);
                
            }else
            {
                return;
            }
            for(int i = 0; i < files.Length; i++)
            {
                files[i] = Path.GetFileName(files[i]);
            }
            comboBox1.Items.AddRange(files);

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //get the specified config file to apply the theme to
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(installPath + comboBox1.Text);
            XmlNode cssNode = xDoc.SelectSingleNode("webreports/general/csstheme");
            
            //if css node is null, this is the first custom theme being added, so we need to create the csstheme node
            if(cssNode == null)
            {
                XmlNode cssTheme = xDoc.CreateNode("element", "csstheme", "");
                cssTheme.InnerText = theme;
                XmlNode general = xDoc.SelectSingleNode("webreports/general");
                general.AppendChild(cssTheme);

            }else
            {
                xDoc.SelectSingleNode("webreports/general/csstheme").InnerText = theme;
            }
            
            //save our changes and let the user know what we changed
            xDoc.Save(installPath + comboBox1.Text);
            System.Windows.Forms.MessageBox.Show(String.Format("Theme {0} created and applied to {1}.", theme, comboBox1.Text));
            this.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
