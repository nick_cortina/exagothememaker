﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Web;
using System.Text.RegularExpressions;
using System.Xml;





namespace ThemeMaker
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void createTheme_Click(object sender, EventArgs e)
        {
            //grab user input
            string exagoPath = textBox1.Text;
            string themeName = textBox2.Text;
            string darkColor = textBox3.Text;
            

            //if any inputs are null or empty, ask user to add those values
            if(String.IsNullOrEmpty(darkColor)  || String.IsNullOrEmpty(themeName) || String.IsNullOrEmpty(exagoPath))
            {

                System.Windows.Forms.MessageBox.Show("Please be sure to include your Exago path, theme name, and a color.");

            }else
            {
                //create app theme with input from the user
                createApplicationTheme(themeName, exagoPath, darkColor);
                // System.Windows.Forms.MessageBox.Show("Theme created.");
                //this.Hide();
                if (checkBox1.Checked)
                {
                    Form2 myForm = new ThemeMaker.Form2(exagoPath, themeName);
                    //make sure the config selector form appears on top of the first form
                    myForm.Show();
                    myForm.Activate();
                    
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show(String.Format("Theme {0} created.", themeName));
                    
                }

            }




            /*
            
                createApplicationTheme(themeName, exagoPath, darkColor, lightColor);
                // System.Windows.Forms.MessageBox.Show("Theme created.");
                if (checkBox1.Checked)
                {
                    Form2 myForm = new ThemeMaker.Form2(exagoPath, themeName);
                    myForm.Show();
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show(String.Format("Theme {0} created.", themeName));
                }
            */
            return;

        }
        public static void DirectoryCopyAll(DirectoryInfo source, DirectoryInfo target)

        {
            if (source.FullName.ToLower() == target.FullName.ToLower())
            {
                return;
            }

            // Check if the target directory exists, if not, create it.
            if (Directory.Exists(target.FullName) == false)
            {
                Directory.CreateDirectory(target.FullName);
            }

            // Copy each file into it's new directory.
            foreach (FileInfo fi in source.GetFiles())
            {
                fi.CopyTo(Path.Combine(target.ToString(), fi.Name), true);
            }

            // Copy each subdirectory using recursion.
            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir = target.CreateSubdirectory(diSourceSubDir.Name);
                DirectoryCopyAll(diSourceSubDir, nextTargetSubDir);
            }

        }
        
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }
        
        private void selectColor1_Click(object sender, EventArgs e)
        {
            DialogResult selectedColor = colorDialog1.ShowDialog();

            if(selectedColor == DialogResult.OK)
            {
                textBox3.Text = ColorTranslator.ToHtml(colorDialog1.Color);
            }
        }
        //added to make sure the process is killed when the program is exited
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }
        /*
        private void updateThemes_Click(object sender, EventArgs e)
        {
            string color1, color2;
            string exagoPath = textBox1.Text;
            //check if Exago Path is null
            if (String.IsNullOrEmpty(exagoPath))
            {

                System.Windows.Forms.MessageBox.Show("Please be sure to include your Exago path.");

            }
            else
            {
                //Get all existing application themes
                DirectoryInfo appThemeDirectoryInfo = new DirectoryInfo(textBox1.Text + @"/ApplicationThemes");
                DirectoryInfo[] styleDirectories = appThemeDirectoryInfo.GetDirectories();

                foreach (DirectoryInfo styleDirectory in styleDirectories)
                {
                    string themeName = styleDirectory.Name;
                    if (themeName != "Basic" && File.Exists(textBox1.Text + @"/ApplicationThemes/" + themeName + "/" + themeName + "_Colors.xml"))
                    {
                        XmlDocument xDoc = new XmlDocument();
                        xDoc.Load(textBox1.Text + @"/ApplicationThemes/" + themeName + "/" + themeName + "_Colors.xml");
                        color1 = xDoc.SelectSingleNode("colors/color1").InnerText;
                        color2 = xDoc.SelectSingleNode("colors/color2").InnerText;
                        xDoc.Save(textBox1.Text + @"/ApplicationThemes/" + themeName + "/" + themeName + "_Colors.xml");
                        createApplicationTheme(themeName, textBox1.Text, color1, color2);

                    }
                }
            }
        }
        */
        /**
         * Method to generate a new application theme. Copies the 'basic' theme and recolors all CSS files and SVG's using the input hex code/color. 
         * @param themeName is the name of the new theme we're creating
         * @param exagoPath is the location of the Exago installation where we'll be saving the theme (theme will be saved in the 'Application Themes' directory)
         * @param darkColor is the new color we use to re-color all CSS elements and SVG icons
         * @return bool indicating success or failure
         * 
         */
        public static bool createApplicationTheme(string themeName, string exagoPath, string darkColor)
        {
            

            if (string.IsNullOrEmpty(darkColor))
            {
                return false;
            }

            //Create the paths and directories for use later
            string basicStylePath = exagoPath + @"/ApplicationThemes/Basic";
            string newStylePath = exagoPath + @"/ApplicationThemes/" + themeName;
            
            DirectoryInfo basicStyleDirectoryInfo = new DirectoryInfo(basicStylePath);
            DirectoryInfo newStyleDirectoryInfo = new DirectoryInfo(newStylePath);
            DirectoryInfo newStyleDirectorySVG = new DirectoryInfo(newStylePath + @"/Images/svg");
            DirectoryInfo newStyleDirectoryCSS = new DirectoryInfo(newStylePath + @"/Css");

            //Check if the user's company already has an Application Theme. If so delete it
            if (Directory.Exists(newStylePath))
            {
                Directory.Delete(newStylePath, true);
            }

            //Copy the Basic Application Theme
            DirectoryCopyAll(basicStyleDirectoryInfo, newStyleDirectoryInfo);
            //ChangePassword all the SVG Icons
            FileInfo[] files = newStyleDirectorySVG.GetFiles();
            foreach (FileInfo file in files)
            {
                //Replace dark Exago orange with color 1 and the light orange with color 2
                File.WriteAllText(file.FullName, File.ReadAllText(file.FullName).Replace("#4E80F4", darkColor).Replace("#4e80f4", darkColor).Replace("#6896ED", darkColor).Replace("#6896ed", darkColor));
            }

            //Change all the CSS files
            files = newStyleDirectoryCSS.GetFiles();
            foreach (FileInfo file in files)
            {
                //Replace dark Exago orange with color 1 and the light orange with color 
                File.WriteAllText(file.FullName, File.ReadAllText(file.FullName).Replace("#4E80F4", darkColor).Replace("#4e80f4", darkColor).Replace("#6896ED", darkColor).Replace("#6896ed", darkColor));
            }

            

            /*
            //write XML file to hold the color values of this theme
            XmlWriter xmlWriter = XmlWriter.Create(newStylePath + "/" + themeName + "_Colors.xml");
            xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement("colors");
            xmlWriter.WriteStartElement("color1");
            xmlWriter.WriteString(darkColor);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndDocument();
            xmlWriter.Close();
            */
            return true;
        }

        
    }
}
